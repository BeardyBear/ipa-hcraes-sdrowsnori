import uuid
from typing import Literal, Optional

from pydantic import BaseModel, Field


class OpenSearchFace(BaseModel):
    # s3 metadata
    id: str
    
    etag: str

    origin_s3_bucket: str
    origin_s3_path: str
    s3_presigned_url: str = None

    media_type: Literal["photo", "video"]
    vector: list[float]

    # bounding box
    left: int
    top: int
    right: int
    bottom: int

    match_score: float = Field(alias="score")
    bb_confidence: Optional[float] = Field(alias="confidence", default=None)

    # frame metadata
    frame_id: Optional[int] = None

class DetectedFace(BaseModel):
    id: str = Field(default_factory=lambda: uuid.uuid4().hex)

    # bounding box
    left: int
    top: int
    right: int
    bottom: int

    confidence: float
    embeddings: Optional[list[float]]

class ImageBase64(BaseModel):
    base64: str


class MatchResult(BaseModel):
    query_face: DetectedFace
    results: list[OpenSearchFace]