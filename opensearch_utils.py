from collections import defaultdict
from typing import Any
import numpy as np

import boto3
from opensearchpy import OpenSearch, RequestsHttpConnection, AWSV4SignerAuth

from models import OpenSearchFace
from config import SETTINGS


def get_k_nearest_neigbhours(vectors: list[np.ndarray],
                             vectors_ids: list[Any],
                             k: int=10,
                             index_name: str=None,
                             client : OpenSearch=None) -> dict[Any, OpenSearchFace]:
    res_batch_size = 1000
    request_batch_size = res_batch_size // k
    metadata = {"index": index_name}
    source = {
        "excludes": ["s3_bucket", "s3_path", "timestamp"]
    }

    faces = defaultdict(list)
    for i in range(0, len(vectors), request_batch_size):
        body = []
        for vec in vectors[i: i + request_batch_size]:
            vec = np.asarray(vec)
            vec /= np.linalg.norm(vec, ord=2)    

            body.append(metadata)
            body.append({"_source": source,
                         "track_scores": True,
                         "size": k,
                         "query": {
                             "knn": {
                                 "vector": {
                                     "vector": vec,
                                     "k": k
                                 }
                             }
                         },
                         "post_filter" :{"match_all": {}}
                        })
            
        msearch_res = client.msearch(index=index_name,
                                     body=body,
                                     max_concurrent_searches=10)
        
        
        for res, q_id in zip(msearch_res["responses"], vectors_ids[i: i + request_batch_size]):
            if res["hits"]["total"]["value"] > 0:
                for item in res["hits"]["hits"]:
                    if item["_source"]["id"] == q_id:
                        continue

                    # return Pydantic model from dict
                    face = OpenSearchFace(**item["_source"],
                                          query_id=q_id,
                                          score=item["_score"] - 1)
                    faces[q_id].append(face)

    return faces

def get_opensearch_client() -> OpenSearch:
    credentials = boto3.Session(aws_access_key_id=SETTINGS.AWS_ACCESS_KEY_ID,
                                aws_secret_access_key=SETTINGS.AWS_SECRET_ACCESS_KEY).get_credentials()
    auth = AWSV4SignerAuth(credentials, SETTINGS.AWS_REGION, "es")
    return OpenSearch(
        timeout=60,
        hosts=[{"host": SETTINGS.OPENSEARCH_HOST, "port": SETTINGS.OPENSEARCH_PORT}],
        http_auth=auth,
        use_ssl=True,
        verify_certs=True,
        connection_class=RequestsHttpConnection,
        pool_maxsize=20,
    )
