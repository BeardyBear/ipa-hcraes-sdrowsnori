import uvicorn
import base64

import cv2
from fastapi import FastAPI
import numpy as np

from models import ImageBase64, MatchResult
from inference import FaceDetection, FaceRecognition, get_faces_from_numpy
from opensearch_utils import get_k_nearest_neigbhours, get_opensearch_client
from utils.aws.s3 import create_presigned_url

from config import SETTINGS

app = FastAPI()


# load image from base64 using opencv
def load_image_from_base64(base64_string: str) -> np.ndarray:
    nparr = np.frombuffer(base64.b64decode(base64_string), np.uint8)
    img_np = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
    return np.array(img_np, dtype=np.float32)
 
DETECTION_MODEL = FaceDetection(model_path=SETTINGS.FACE_DETECTION_MODEL_PATH)
RECOGNITION_MODEL = FaceRecognition(model_path=SETTINGS.FACE_RECOGNITION_MODEL_PATH)
OS_CLIENT = get_opensearch_client()


# fastapi endpoint that excepts image base64, loads it, detects faces, and returning k nearest neighbours
@app.post("/face_match")
async def get_knn(image: ImageBase64) -> list[MatchResult]:
    image = load_image_from_base64(image.base64)
    faces = get_faces_from_numpy(image, DETECTION_MODEL, RECOGNITION_MODEL)
    id2face = {face.id: face for face in faces}

    # supposed to be only one face
    vectors = [face.embeddings for face in faces]
    ids = [face.id for face in faces]
    # filter results by threshold
    matched_faces = get_k_nearest_neigbhours(vectors, ids, k=SETTINGS.K, index_name=SETTINGS.INDEX_NAME, client=OS_CLIENT)

    res = []
    for query_face_id, matches in matched_faces.items():
        final_matches = []
        for match in matches:
            if match.match_score < SETTINGS.THRESHOLD:
                continue
            match.s3_presigned_url = create_presigned_url(match.origin_s3_bucket, match.origin_s3_path)
            final_matches.append(match)
        res.append(MatchResult(query_face=id2face[query_face_id], results=final_matches))
    return res


if __name__ == "__main__":
    uvicorn.run(app, host=SETTINGS.SERVER_HOST, port=SETTINGS.SERVER_PORT)
    