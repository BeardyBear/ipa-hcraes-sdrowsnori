## Prerequisites

Make sure you have the following prerequisites installed on your system:

- Python (version >= 3.11)
- Poetry

## Installation

### Install Poetry

Poetry is a dependency manager for Python. It also provides packaging and publishing tools.

To install Poetry, open a terminal and run:

```curl -sSL https://install.python-poetry.org | python3 -```

After the installation run the following

```cd server```

```poetry install```

## Usage

### Set env vars

```bash
export AWS_REGION="us-east-1"
export AWS_ACCESS_KEY_ID="..."
export AWS_SECRET_ACCESS_KEY="..."
export OPENSEARCH_HOST="..."
```

### Start server

```poetry run python server.py```

The server will be start at `localhost:8000`

api docs are available at `localhost:8000/docs`

you can change the server setings in config.py or pass them as environment variable

input example:
POST localhost:8000/face_match
```json
{
  "base64": "your base64 image here"
}
```


output:
```json
[
  {
    "query_face": {
      "id": "7c43b8dc323646a7b4e4de2414294e8f",
      "left": 100,
      "top": 5,
      "right": 150,
      "bottom": 50,
      "confidence": 0.888,
      "embeddings": [
        0.36,
        0.8995,
        0.3266,
        ..
      ]
    },
    "results": [
      {
        "id": "5912cbdea2f54ee7a834c4f2cbc1d81e",
        "etag": "04e1a16378d44aab9d532a57b9aaab38",
        "origin_s3_bucket": "bucket",
        "origin_s3_path": "s3://bucket",
        "media_type": "photo",
        "vector": [
        0.316,
        0.7995,
        0.3466,
        ..
      ],
        "left": 100,
        "top": 5,
        "right": 150,
        "bottom": 50,
        "score": 0.8,
        "confidence": 0.2,
        "frame_id": None
      },
      ...
    ]
  }
] 
```
