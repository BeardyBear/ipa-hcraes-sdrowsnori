import cv2
import numpy as np
from models import DetectedFace


CONFIDENCE_POSITION = 14
MAX_IMAGE_RESOLUTION = 2_000_000  # 2 MP
TARGET_RESIZE_SIZE = (1400, 1400)


class FaceDetection:
    def __init__(self, model_path: str):
        self.face_detector = cv2.FaceDetectorYN_create(
            model_path,
            "",
            input_size=(0, 0),
            score_threshold=0.6,
            nms_threshold=0.3,
            backend_id=cv2.dnn.DNN_BACKEND_CUDA,
            target_id=cv2.dnn.DNN_TARGET_CUDA,
        )

    def detect_faces(self, img: np.ndarray) -> np.ndarray:
        height, width, _ = img.shape
        self.face_detector.setInputSize((width, height))
        _, faces = self.face_detector.detect(img)
        if faces is None:
            return np.empty(0)
        return faces


class FaceRecognition:
    def __init__(self, model_path: str):
        self.face_recognizer = cv2.FaceRecognizerSF_create(model_path, "",
                                                           backend_id=cv2.dnn.DNN_BACKEND_OPENCV,
                                                           target_id=cv2.dnn.DNN_TARGET_CPU)

    def get_features(self, orig_img: np.ndarray, face: np.ndarray) -> np.ndarray:
        aligned_face = self.face_recognizer.alignCrop(orig_img, face)
        return self.face_recognizer.feature(aligned_face)[0]
    

def rescale_faces(faces: np.ndarray, original_width: int, resized_width: int) -> np.ndarray:
    """
    rescale faces to original image dimensions after the face detection has been applied to a resized image
    """
    if faces.size == 0:
        return faces
    
    # Calculate scaling factors
    scale_factor = original_width / resized_width
    faces[:, :-1] = faces[:, :-1] * scale_factor

    return faces


def resize_keep_aspect_ratio(image: np.ndarray, target_size: tuple[int, int]) -> np.ndarray:
    image_height, image_width = image.shape[:2]
    target_width, target_height = target_size

    # Calculate scaling factors
    width_scale = target_width / image_width
    height_scale = target_height / image_height
    scale = min(width_scale, height_scale)

    # Resize image
    resized_image = cv2.resize(image, None, fx=scale, fy=scale, interpolation=cv2.INTER_NEAREST)

    return resized_image


def get_faces_from_numpy(
    image: np.ndarray,
    detection_model: FaceDetection,
    recognition_model: FaceRecognition,
) -> list[DetectedFace]:
    
    if image.shape[0] * image.shape[1] > MAX_IMAGE_RESOLUTION:
        resized = resize_keep_aspect_ratio(image, TARGET_RESIZE_SIZE)
        raw_faces = detection_model.detect_faces(resized)
        raw_faces = rescale_faces(raw_faces, image.shape[0], resized.shape[0])
    else:
        raw_faces = detection_model.detect_faces(image)

    image_faces = []
    for raw_face in raw_faces:
        face_embedding = recognition_model.get_features(image, raw_face)
        face = DetectedFace(
            left=int(raw_face[0]), # x
            top=int(raw_face[1]), # y
            right=int(raw_face[0]) + int(raw_face[2]), # x + w
            bottom=int(raw_face[1]) + int(raw_face[3]), # y + h
            embeddings=face_embedding,
            confidence=raw_face[CONFIDENCE_POSITION],
        )
        image_faces.append(face)
    return image_faces
