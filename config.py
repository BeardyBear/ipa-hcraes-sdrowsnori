from pydantic import Field
from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    # server host and port
    SERVER_HOST: str = Field(default="localhost")
    SERVER_PORT: int = Field(default=8000)
    INDEX_NAME: str = Field(default="face")

    AWS_REGION: str = Field(default="us-east-1")
    AWS_ACCESS_KEY_ID: str = Field(default="")
    AWS_SECRET_ACCESS_KEY: str = Field(default="")
    
    # opensearch host and port
    OPENSEARCH_HOST: str = Field(default="localhost")
    OPENSEARCH_PORT: int = Field(default=443)

    # face detection model path
    FACE_DETECTION_MODEL_PATH: str = Field(default="./models/face_detection_yunet_2023mar.onnx")
    # face recognition model path
    FACE_RECOGNITION_MODEL_PATH: str = Field(default="./models/face_recognition_sface_2021dec.onnx")

    # k nearest neighbours
    K: int = Field(default=10)
    # threshold for filtering results
    THRESHOLD: float = Field(default=0.0)
    

SETTINGS = Settings()